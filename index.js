const express = require('Express');
const app = express();
const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get('/home', (request, response) => {
  response.send('Welcome to homepage');
});

let users = [
  {
    id: 1,
    username: 'johndoe',
    password: 'johndoe1234',
  },
  {
    id: 2,
    username: 'test',
    password: 'testsample',
  },
];

app.get('/users', (request, response) => {
  response.send(users);
});

app.post('/adduser', (request, response) => {
  console.log(request.body);

  if (request.body.username !== '' && request.body.password !== '') {
    users.push(request.body);
    console.log(users);
    response.send(`User ${request.body.username} is successfully registered!`);
  } else {
    response.send('Please input BOTH username and password');
  }
});

app.delete('/delete-user', (request, response) => {
  for (let index = 0; index < users.length; index++) {
    if (request.body.username == users[index].username) {
      users.splice(index, 1);
      response.send(`User ${request.body.username} has been deleted.`);
      break;
    }
  }
});

app.listen(port, () => console.log(`Server running at localhost:${port}`));
